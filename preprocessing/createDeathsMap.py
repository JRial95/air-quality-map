import json
import re
import pandas
from collections import OrderedDict


# load numbers of deaths attributable to ambient air pollution
# per 100k inhabitants of a country during a year,
# for various countries in 2016
def load_countries_air_pollution_deaths_data():
  with open("deaths.100k.data.json", "r", encoding='utf-8') as f:
    air_pollution_deaths_per_100k = json.load(f)
  return air_pollution_deaths_per_100k


# load geographic data structures such as borders of various countries
def load_countries_geo_data():
  with open("countries.geo.json", "r", encoding='utf-8') as f:
    countries_geo_data = json.load(f)
  return countries_geo_data


# load number of deaths per 1k inhabitants of a country during a year,
# for various countries and years (including 2016)
def load_countries_total_deaths_data():
    total_deaths = pandas.read_csv("deaths.world.csv", header=0)
    total_deaths = total_deaths[['Country Name', '2016']]
    total_deaths = total_deaths.set_index('Country Name')
    return total_deaths


# returns geographic data about the country or None if country not found
def get_country_geo_data(country_name, countries_geo_data):
    country_geo_data = None
    for item in countries_geo_data["features"]:
        if item["properties"]["ADMIN"] == country_name:
            country_geo_data = item
    return country_geo_data


# returns number of deaths per 100k inhabitants of the country in 2016, None if not found
def get_country_deaths_nb_per_100k(country_name, countries_total_deaths_data):
    try:
        country_deaths_nb_per_1k = countries_total_deaths_data.loc[country_name, '2016']
        country_deaths_nb_per_100k = 100 * country_deaths_nb_per_1k
        return country_deaths_nb_per_100k
    except KeyError:
        return None


countries_air_pollution_deaths_data = load_countries_air_pollution_deaths_data()
countries_geo_data = load_countries_geo_data()
countries_total_deaths_data = load_countries_total_deaths_data()

# change country names in countries_geo_data so that they match their counterpart in countries_air_pollution_deaths_data
for item in countries_geo_data["features"]:
    country_name = item["properties"]["ADMIN"]

    if country_name == "The Bahamas":
        item["properties"]["ADMIN"] = "Bahamas"
    elif country_name == "Bolivia":
        item["properties"]["ADMIN"] = "Bolivia (Plurinational State of)"
    elif country_name == "Brunei":
        item["properties"]["ADMIN"] = "Brunei Darussalam"
    elif country_name == "Cape Verde":
        item["properties"]["ADMIN"] = "Cabo Verde"
    elif country_name == "Republic of Congo":
        item["properties"]["ADMIN"] = "Congo"
    elif country_name == "Ivory Coast":
        item["properties"]["ADMIN"] = "Côte d'Ivoire"
    elif country_name == "Czech Republic":
        item["properties"]["ADMIN"] = "Czechia"
    elif country_name == "North Korea":
        item["properties"]["ADMIN"] = "Democratic People's Republic of Korea"
    elif country_name == "Swaziland":
        item["properties"]["ADMIN"] = "Eswatini"
    elif country_name == "Guinea Bissau":
        item["properties"]["ADMIN"] = "Guinea-Bissau"
    elif country_name == "Iran":
        item["properties"]["ADMIN"] = "Iran (Islamic Republic of)"
    elif country_name == "Laos":
        item["properties"]["ADMIN"] = "Lao People's Democratic Republic"
    elif country_name == "Federated States of Micronesia":
        item["properties"]["ADMIN"] = "Micronesia (Federated States of)"
    elif country_name == "South Korea":
        item["properties"]["ADMIN"] = "Republic of Korea"
    elif country_name == "Moldova":
        item["properties"]["ADMIN"] = "Republic of Moldova"
    elif country_name == "Russia":
        item["properties"]["ADMIN"] = "Russian Federation"
    elif country_name == "Republic of Serbia":
        item["properties"]["ADMIN"] = "Serbia"
    elif country_name == "Syria":
        item["properties"]["ADMIN"] = "Syrian Arab Republic"
    elif country_name == "Macedonia":
        item["properties"]["ADMIN"] = "The former Yugoslav republic of Macedonia"
    elif country_name == "East Timor":
        item["properties"]["ADMIN"] = "Timor-Leste"
    elif country_name == "United Kingdom":
        item["properties"]["ADMIN"] = "United Kingdom of Great Britain and Northern Ireland"
    elif country_name == "Venezuela":
        item["properties"]["ADMIN"] = "Venezuela (Bolivarian Republic of)"
    elif country_name == "Vietnam":
        item["properties"]["ADMIN"] = "Viet Nam"

# change country names in countries_total_deaths_data so that they match their counterpart
# in countries_air_pollution_deaths_data
country_names_in_total_deaths = countries_total_deaths_data.index.tolist()

# update country_names_in_total_deaths
for (i, country_name) in enumerate(country_names_in_total_deaths):
    if country_name == "The Bahamas":
        country_names_in_total_deaths[i] = "Bahamas"
    elif country_name == "Bolivia":
        country_names_in_total_deaths[i] = "Bolivia (Plurinational State of)"
    elif country_name == "Brunei":
        country_names_in_total_deaths[i] = "Brunei Darussalam"
    elif country_name == "Cape Verde":
        country_names_in_total_deaths[i] = "Cabo Verde"
    elif country_name == "Republic of Congo":
        country_names_in_total_deaths[i] = "Congo"
    elif country_name == "Ivory Coast":
        country_names_in_total_deaths[i] = "Côte d'Ivoire"
    elif country_name == "Czech Republic":
        country_names_in_total_deaths[i] = "Czechia"
    elif country_name == "North Korea":
        country_names_in_total_deaths[i] = "Democratic People's Republic of Korea"
    elif country_name == "Swaziland":
        country_names_in_total_deaths[i] = "Eswatini"
    elif country_name == "Guinea Bissau":
        country_names_in_total_deaths[i] = "Guinea-Bissau"
    elif country_name == "Iran":
        country_names_in_total_deaths[i] = "Iran (Islamic Republic of)"
    elif country_name == "Laos":
        country_names_in_total_deaths[i] = "Lao People's Democratic Republic"
    elif country_name == "Federated States of Micronesia":
        country_names_in_total_deaths[i] = "Micronesia (Federated States of)"
    elif country_name == "South Korea":
        country_names_in_total_deaths[i] = "Republic of Korea"
    elif country_name == "Moldova":
        country_names_in_total_deaths[i] = "Republic of Moldova"
    elif country_name == "Russia":
        country_names_in_total_deaths[i] = "Russian Federation"
    elif country_name == "Republic of Serbia":
        country_names_in_total_deaths[i] = "Serbia"
    elif country_name == "Syria":
        country_names_in_total_deaths[i] = "Syrian Arab Republic"
    elif country_name == "Macedonia":
        country_names_in_total_deaths[i] = "The former Yugoslav republic of Macedonia"
    elif country_name == "East Timor":
        country_names_in_total_deaths[i] = "Timor-Leste"
    elif country_name == "United Kingdom":
        country_names_in_total_deaths[i] = "United Kingdom of Great Britain and Northern Ireland"
    elif country_name == "Venezuela":
        country_names_in_total_deaths[i] = "Venezuela (Bolivarian Republic of)"
    elif country_name == "Vietnam":
        country_names_in_total_deaths[i] = "Viet Nam"

countries_total_deaths_data.index = country_names_in_total_deaths

# get list of countries without duplicates
country_names_in_air_pollution_deaths = [item["dims"]["COUNTRY"] for item in countries_air_pollution_deaths_data["fact"]]
country_names_in_air_pollution_deaths = list(OrderedDict.fromkeys(country_names_in_air_pollution_deaths))

# list countries which are found in country_names_in_air_pollution_deaths & which cannot be found in countries_geo_data
country_names_in_geo_data = [item["properties"]["ADMIN"] for item in countries_geo_data["features"]]

for country_name in country_names_in_air_pollution_deaths:
    if country_name not in country_names_in_geo_data:
        print("Not found", country_name, "in geo data")


# list countries which are found in country_names_in_air_pollution_deaths &
# which cannot be found in countries_total_deaths_data
country_names_in_total_deaths = countries_total_deaths_data.index.tolist()

for country_name in country_names_in_air_pollution_deaths:
    if country_name not in country_names_in_total_deaths:
        print("Not found", country_name, "in total deaths data")

# for each country, create a feature dictionary which stores data
# related to number of deaths and append it to the features list
features = []

for (i, country_name) in enumerate(country_names_in_air_pollution_deaths):
    country_geo_data = get_country_geo_data(country_name, countries_geo_data)
    country_deaths_nb_per_100k = get_country_deaths_nb_per_100k(country_name, countries_total_deaths_data)

    if country_geo_data is None:
        print("Not found", country_name, "in geo data")
        continue

    if country_deaths_nb_per_100k is None:
        print("Not found", country_name, "in total deaths data")
        continue

    feature = dict(
        type="Feature",
        id=str(i + 1),
        properties=dict(
            name=country_name,
            country_deaths_nb_per_100k=round(country_deaths_nb_per_100k, 3),
            country_deaths_by_air_pollution_nb_per_100k=-1.0,
            country_deaths_by_lower_respiratory_infections_nb_per_100k=-1.0,
            country_deaths_by_trachea_bronchus_lung_cancers_nb_per_100k=-1.0,
            country_deaths_by_ischaemic_heart_disease_nb_per_100k=-1.0,
            country_deaths_by_stroke_nb_per_100k=-1.0,
            country_deaths_by_chronic_obstructive_pulmonary_disease_nb_per_100k=-1.0
        ),
        geometry=country_geo_data["geometry"])

    features.append(feature)

# for each country, update numbers of deaths for country
for item in countries_air_pollution_deaths_data["fact"]:
    country_name = item["dims"]["COUNTRY"]

    # get corresponding feature in features if it exists
    country_features = [feature for feature in features if feature["properties"]["name"] == country_name]

    if len(country_features) != 1:
        print("the country_features of " + country_name + " does not have exactly one item")
        continue

    feature = country_features[0]

    # update the feature with deaths numbers corresponding to ENVCAUSE
    death_cause = item["dims"]["ENVCAUSE"]
    deaths_nb = int(re.sub("\ \[.*?\]", "", item["Value"]))

    if death_cause == "Total":
        feature["properties"]["country_deaths_by_air_pollution_nb_per_100k"] = round(deaths_nb, 3)
    elif death_cause == "Lower respiratory infections":
        feature["properties"]["country_deaths_by_lower_respiratory_infections_nb_per_100k"] = round(deaths_nb, 3)
    elif death_cause == "Trachea, bronchus, lung cancers":
        feature["properties"]["country_deaths_by_trachea_bronchus_lung_cancers_nb_per_100k"] = round(deaths_nb, 3)
    elif death_cause == "Ischaemic heart disease":
        feature["properties"]["country_deaths_by_ischaemic_heart_disease_nb_per_100k"] = round(deaths_nb, 3)
    elif death_cause == "Stroke":
        feature["properties"]["country_deaths_by_stroke_nb_per_100k"] = round(deaths_nb, 3)
    elif death_cause == "Chronic obstructive pulmonary disease":
        feature["properties"]["country_deaths_by_chronic_obstructive_pulmonary_disease_nb_per_100k"] = round(deaths_nb, 3)

print(features[68])

all_data = dict(
    type = "FeatureCollection",
    features = features)

with open("deaths.map.json", "w") as deaths_map:
    json.dump(all_data, deaths_map)
