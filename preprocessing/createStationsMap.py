import pandas as pd
import unidecode
import json

# Fetch stations data
print("Fetch stations data...")
data_stations = pd.read_excel("WHO_AAP_database_May2016_v3web.xlsx", sheet_name="database", header=None, skiprows=3)
stations = data_stations.values

# Fetch world cities data
print("Fetch world cities data...")
data_cities = pd.read_csv("worldcitiespop.csv", encoding="ISO-8859-1", low_memory=False) 
cities_values = data_cities.values
countries_of_cities = set(cities_values[:,0])
cities = dict()
for country in countries_of_cities:
    print(" ", country)
    sub_cities = [x for x in cities_values if x[0]==country]
    sub_cities_zip = [list(x) for x in zip(*sub_cities)]
    cities[country] = dict(zip(sub_cities_zip[1], sub_cities))

# Fetch Country <-> ISO data
print("Fetch Country <-> ISO data...")
data_countries = pd.read_csv("country_iso.csv") 
countries_values = data_countries.values
countries_name = [x.lower() for x in countries_values[:,0]]
countries = dict(zip(countries_name, countries_values[:,1]))

# Array of data
data = []

# Searching
print("Searching...")
for station in stations:
    # Find ISO of the country
    country_name = station[3].lower()
    country_iso = countries.get(country_name, None)

    if not country_iso:
        print("ISO for the country", country_name, "not found!")

    city_name = station[4].lower().split(",")[0]
    city_name = unidecode.unidecode(city_name)

    sub_cities = cities.get(country_iso.lower(), None)

    if not sub_cities:
        print("Sub cities for the country", country_iso, "not found!")
    else:
        position = sub_cities.get(city_name, None)
        if position is None:
            print("Position for the city", city_name, "not found!")
        else:
            print("Position for the city", city_name, "position:", position)
            data.append(
                dict(
                    name=position[2],
                    latitude=position[5],
                    longitude=position[6],
                    pm10=station[5],
                    pm25=station[9]
                )
            )

# Save data
with open("stations_data.json", "w") as stations_data:
    json.dump(dict(stations=data), stations_data)