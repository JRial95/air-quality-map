

# Air Quality Map

## Motivation

Cette application web a pour but de présenter de manière interactive des informations relatives aux morts dus à la qualité de l'air ambiant par pays. 
L'organisation mondiale de la santé présente ces informations sous forme tabulaire, ce qui rend la comparaison de ces données plutôt compliquée. 
Nous avons décidé de représenter ces donnés de manière cartographique / système GIS. 
Cette représentation doit permettre à l'utilisateur de comparer très rapidement la qualité de l'air et les cause de décès dû à la pollution entre différents pays. 
Le public cible de cette application web est toute personne intéressée par la qualité de l'air ambiant.

## Données 

Nous avons utilisé un dataset de l'organisation mondiale de la santé détaillait le nombre de morts dû à la pollution de l'air en 2016.

Le dataset contient pour chaque pays les informations suivantes: 

- le nombre de morts dus à la pollution.
  - et un détail par maladie
    - Lowere respiratory infection
    - Trachea, bronchus, lung cancers   
    - Ischaemic heart disease    
    - Stroke   
    - Chronic obstructive pulmonary disease
    
Ces données sont normalisées pour 100k personnes.

 [Lien vers Dataset](http://apps.who.int/gho/data/node.main.BODAMBIENTAIRDTHS?lang=en)

Un autre dataset détaillant le nombre de morts globaux par pays en 2016 a également été utilisé pour calculer le taux de mortalité en lien avec la qualité de l'air. [lien](https://data.worldbank.org/indicator/SP.DYN.CDRT.IN?end=2016&start=2016)

## Visualisation

### Visualisation cartographique

La vue principale de l'application est une carte du monde. Cette vue "teinte" chaque pays en fonction du taux de mortalité dû à la pollution. 
Ainsi un pays, avec un taux élevé sera teinté plus foncé qu'un pays avec taux faible.
Cette vue principale permet de se déplacer et de zoomer sur la carte du monde.

Les interractions suivantes sont possibles avec la carte:

* Un clique sur un pays affiche un détail sur ce dernier dans une modale.
* Lorsqu'on passe la souris sur un pays, la légende en haut à droite affiche les pourcentages de mortalité par cause dans une légende.


### Filtres

Des filtres par causes de morts sont également disponibles. Ces filtres permettant par exemple de répondre à la question "dans quel pays la cause de mortalité "cancer du poumon dû à la pollution de l'air ambiant" sont la plus importante .".

### Détails par pays

Lorsque l'on clique sur un pays, un modal apparaît et affiche un pie chart détaillant les donnés de mortalité due à la qualité de l'air relative au pays.


## Améliorations

* Ajouter une informations sur la source des données sur le UI
* Les teintes de colorations des pays pourraient être plus distinctes, il est parfois relativement compliqué de différencier lequel est le plus foncé.
* Garder concordance entre les couleurs du layer de la map, et celles du piechart.

## AirQualityMap - Project Setup

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 6.2.4.

### Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

### Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

### Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.
