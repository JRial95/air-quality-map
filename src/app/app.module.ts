import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {AppComponent} from './app.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {
  MatButtonModule,
  MatDialogModule,
  MatFormFieldModule,
  MatIconModule,
  MatSelectModule,
  MatToolbarModule
} from '@angular/material';
import {LeafletModule} from '@asymmetrik/ngx-leaflet';
import {DeathsService} from './service/deaths.service';
import {HttpClientModule} from '@angular/common/http';
import {InformationDialogComponent} from './information-dialog/information-dialog.component';
import {CountryDetailsDialogComponent} from './country-details-dialog/country-details-dialog.component';
import {StationsService} from "./service/stations.service";

@NgModule({
  declarations: [
    AppComponent,
    InformationDialogComponent,
    CountryDetailsDialogComponent,
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    BrowserAnimationsModule,
    MatButtonModule,
    MatIconModule,
    MatToolbarModule,
    MatFormFieldModule,
    MatSelectModule,
    LeafletModule.forRoot(),
    MatDialogModule,
  ],
  providers: [
    DeathsService,
    StationsService
  ],
  entryComponents: [
    InformationDialogComponent,
    CountryDetailsDialogComponent,
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
