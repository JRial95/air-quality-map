import {Component, Inject, OnInit} from '@angular/core';
import * as Chart from 'chart.js';
import {MAT_DIALOG_DATA} from '@angular/material';


@Component({
  selector: 'app-country-details-dialog',
  templateUrl: './country-details-dialog.component.html',
  styleUrls: ['./country-details-dialog.component.css']
})
export class CountryDetailsDialogComponent implements OnInit {

  chart: Chart = null;


  constructor(@Inject(MAT_DIALOG_DATA) public data: any) {
  }

  ngOnInit() {
    const chartConfiguration: Chart.ChartConfiguration = {
      type: 'pie',
      data: {
        labels: ['Infection respiratoire basse', 'Cancer du poumon', 'Maladie coronarienne', 'AVC', 'Bronchopneumopathie chronique obstructive'],
        datasets: [
          {
            backgroundColor: ['#3e95cd', '#8e5ea2', '#3cba9f', '#e8c3b9', '#c45850'],
            data: [
              this.data.property.country_deaths_by_lower_respiratory_infections_nb_per_100k,
              this.data.property.country_deaths_by_trachea_bronchus_lung_cancers_nb_per_100k,
              this.data.property.country_deaths_by_ischaemic_heart_disease_nb_per_100k,
              this.data.property.country_deaths_by_stroke_nb_per_100k,
              this.data.property.country_deaths_by_chronic_obstructive_pulmonary_disease_nb_per_100k,
            ]
          }
        ]
      },
      options: {
        title: {
          display: true,
          text: 'Causes de morts dus à la pollution de l\'air (Pour 100\'000 personnes)'
        }
      }
    };

    this.chart = new Chart('canvas', chartConfiguration);
  }

}
