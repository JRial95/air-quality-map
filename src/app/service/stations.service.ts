import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";

@Injectable()
export class StationsService {

  private static handleError(error: any): Promise<any> {
    return Promise.reject(error);
  }

  constructor(private _httpClient: HttpClient) {
  }

  public getData(): Promise<any> {
    return this._httpClient
      .get('assets/stations_data.json')
      .toPromise()
      .catch(StationsService.handleError);
  }
}
