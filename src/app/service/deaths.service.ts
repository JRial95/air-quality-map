import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";

@Injectable()
export class DeathsService {

  private static handleError(error: any): Promise<any> {
    return Promise.reject(error);
  }

  constructor(private _httpClient: HttpClient) {
  }

  public getData(): Promise<any> {
    return this._httpClient
      .get('assets/deaths_by_country.json')
      .toPromise()
      .catch(DeathsService.handleError);
  }
}
