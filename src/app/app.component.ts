import {Component, NgZone, OnInit, ViewChild} from '@angular/core';
import {circle, Control, DomUtil, GeoJSON, geoJSON, LatLng, latLng, LayerGroup, PathOptions, tileLayer} from 'leaflet';
import {DeathsService} from './service/deaths.service';
import {LeafletDirective} from '@asymmetrik/ngx-leaflet';
import {MatDialog, MatSelectChange} from '@angular/material';
import {InformationDialogComponent} from './information-dialog/information-dialog.component';
import {CountryDetailsDialogComponent} from './country-details-dialog/country-details-dialog.component';
import {StationsService} from "./service/stations.service";

export interface PollutionProperties {
  value: string;
  viewValue: string;
}

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {

  public constructor(private _deathsService: DeathsService, private _stationsService: StationsService,
                     public dialog: MatDialog, private ngZone: NgZone) {
    this.deathsService = _deathsService;
  }

  private static infoDiv: HTMLElement;
  private static legendDiv: HTMLElement;
  private static min: number;
  private static max: number;

  private static selectedFeature = 'country_deaths_by_air_pollution_nb_per_100k';
  private polutionDeathGeoJSON: GeoJSON;
  private death_data: any;

  private deathsService: DeathsService;
  @ViewChild(LeafletDirective) public leaflet;
  public options: any = {
    layers: [
      tileLayer('http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png',
        {maxZoom: 18, attribution: '© <a href="https://www.openstreetmap.org">OpenStreetMap</a> contributors'})
    ],
    zoom: 3,
    center: latLng(46.94809, 7.44744),
  };

  pollutionProperties: PollutionProperties[] = [
    // {value: 'country_deaths_nb_per_100k', viewValue: 'Total Death'},
    {value: 'country_deaths_by_air_pollution_nb_per_100k', viewValue: 'Général'},
    {value: 'country_deaths_by_lower_respiratory_infections_nb_per_100k', viewValue: 'Infection respiratoire basse'},
    {value: 'country_deaths_by_ischaemic_heart_disease_nb_per_100k', viewValue: 'Maladie coronarienne'},
    {value: 'country_deaths_by_trachea_bronchus_lung_cancers_nb_per_100k', viewValue: 'Cancer du poumon'},
    {value: 'country_deaths_by_stroke_nb_per_100k', viewValue: 'AVC'},
    {
      value: 'country_deaths_by_chronic_obstructive_pulmonary_disease_nb_per_100k',
      viewValue: 'Bronchopneumopathie chronique obstructive'
    },
  ];

  public selectedValue = 'country_deaths_by_air_pollution_nb_per_100k';

  // Static
  private static highlightFeature(e: any): void {
    const layer: any = e.layer;
    AppComponent.updateInfo(layer.feature.properties);
  }

  private static resetHighlight(): void {
    AppComponent.updateInfo();
  }

  private static getInfo(): Control {
    const info: Control = new Control();

    info.onAdd = (): HTMLElement => {
      AppComponent.infoDiv = DomUtil.create('div', 'info');
      AppComponent.updateInfo();
      return AppComponent.infoDiv;
    };

    return info;
  }

  private static updateInfo(prop?: any): void {
    if (prop) {
      AppComponent.infoDiv.innerHTML = '<h4>' + prop.name + '</h4>';
      AppComponent.infoDiv.innerHTML += '<b>Morts dus à la pollution de l\'air: </b>' + (prop.country_deaths_by_air_pollution_nb_per_100k / prop.country_deaths_nb_per_100k * 100).toFixed(3) + '%<br>';
      AppComponent.infoDiv.innerHTML += '<b>';
      AppComponent.infoDiv.innerHTML += '<b>En Détails:</b><br>';
      AppComponent.infoDiv.innerHTML += '<b>Infection respiratoire basse: </b>' + (prop.country_deaths_by_lower_respiratory_infections_nb_per_100k / prop.country_deaths_nb_per_100k * 100).toFixed(3) + '%<br>';
      AppComponent.infoDiv.innerHTML += '<b>Maladie coronarienne: </b>' + (prop.country_deaths_by_ischaemic_heart_disease_nb_per_100k / prop.country_deaths_nb_per_100k * 100).toFixed(3) + '%<br>';
      AppComponent.infoDiv.innerHTML += '<b>Cancer du poumon: </b>' + (prop.country_deaths_by_trachea_bronchus_lung_cancers_nb_per_100k / prop.country_deaths_nb_per_100k * 100).toFixed(3) + '%<br>';
      AppComponent.infoDiv.innerHTML += '<b>AVC: </b>' + (prop.country_deaths_by_stroke_nb_per_100k / prop.country_deaths_nb_per_100k * 100).toFixed(3) + '%<br>';
      AppComponent.infoDiv.innerHTML += '<b>Bronchopneumopathie chronique obstructive: </b>' + (prop.country_deaths_by_chronic_obstructive_pulmonary_disease_nb_per_100k / prop.country_deaths_nb_per_100k * 100).toFixed(3) + '%';
    } else {
      AppComponent.infoDiv.innerHTML = '';
    }
  }

  private static getLegend(): Control {
    const legend: Control = new Control({position: 'bottomright'});

    legend.onAdd = (): HTMLElement => {
      AppComponent.legendDiv = DomUtil.create('div', 'info legend');
      AppComponent.updateLegend();
      return AppComponent.legendDiv;
    };

    return legend;
  }

  private static updateLegend(): void {
    const div: HTMLElement = AppComponent.legendDiv;
    div.innerHTML = '<h4>Légende</h4>';
    const grades: number[] = [];
    const step = AppComponent.max / 4;
    for (let i = 0; i < 5; i++) {
      grades.push((step * i));
    }
    const grades_string = grades.map(g => (g * 100).toFixed(2) + '%');

    for (let i = 0; i < grades.length; i++) {
      div.innerHTML +=
        '<i style="background:' + AppComponent.getColor(grades[i]) + '"></i>' +
        grades_string[i] + (grades_string[i + 1] ? ' &ndash; ' + (grades_string[i + 1]) + '<br>' : ' +');
    }
  }

  private static getColor(d): string {
    const perc = (d - AppComponent.min) / (AppComponent.max - AppComponent.min);
    const hue = ((1 - perc) * 120).toString(10);
    return ['hsl(', hue, ',100%,50%)'].join('');
  }

  private static style(feature?): PathOptions {
    const ratio = feature.properties[AppComponent.selectedFeature] / feature.properties.country_deaths_nb_per_100k;
    return {
      fillColor: AppComponent.getColor(ratio),
      weight: 1,
      color: 'black',
      fillOpacity: 0.7
    };
  }

  ngOnInit() {
    this._deathsService.getData().then(data => {
      this.death_data = data;
      const deaths_by_pollution_ratio_t = data.features.map(
        c => c.properties.country_deaths_by_air_pollution_nb_per_100k / c.properties.country_deaths_nb_per_100k);
      AppComponent.max = Math.max(...deaths_by_pollution_ratio_t);
      AppComponent.min = Math.min(...deaths_by_pollution_ratio_t);

      this.polutionDeathGeoJSON = geoJSON(data, {style: AppComponent.style});
      this.polutionDeathGeoJSON.on({
        mouseover: AppComponent.highlightFeature,
        mouseout: AppComponent.resetHighlight,
        click: (e: any) => {
          this.ngZone.run(() => this.dialog.open(CountryDetailsDialogComponent,
            {
              width: '700px',
              height: '500px',
              data: {
                property: e.layer.feature.properties
              }
            }))
          ;
        },
      });

      this.polutionDeathGeoJSON.addTo(this.leaflet.getMap());
      AppComponent.getLegend().addTo(this.leaflet.getMap());
      AppComponent.getInfo().addTo(this.leaflet.getMap());

      this._stationsService.getData().then(data => {
        const layerGroup = new LayerGroup();
        // add some fake stations to validate the functions
        for (let station of data.stations) {
          AppComponent.addCircle(
            latLng(
              station.latitude,
              station.longitude
            ), layerGroup, station);
        }
        this.leaflet.getMap().addLayer(layerGroup);
      });

      this.openDialog();

    });
  }

  openDialog() {
    this.dialog.closeAll();
    this
      .dialog
      .open(InformationDialogComponent);
  }

  private static addCircle(latlng: LatLng, layerGroup: LayerGroup, station: any) {
    circle(latlng, {
      fillColor: '#4559ff',
      radius: 5000
    }).bindPopup('<h4>' + station.name + '</h4>'
    + '<b>PM10 (ug/m<sup>3</sup>): </b>' + Math.round(station.pm10 * 100) / 100 + '<br>'
    + '<b>PM2.5 (ug/m<sup>3</sup>): </b>' + Math.round(station.pm25 * 100) / 100)
      .openPopup().addTo(layerGroup);
  }

  public applyChange(e: MatSelectChange) {
    AppComponent.selectedFeature = e.value;
    const values = this.death_data.features.map(
      c => {
        return c.properties[AppComponent.selectedFeature] / c.properties.country_deaths_nb_per_100k;
      });
    AppComponent.max = Math.max(...values);
    AppComponent.min = Math.min(...values);
    AppComponent.updateLegend();
    const a: any = {};
    this.polutionDeathGeoJSON.setStyle(AppComponent.style as any);
  }
}
